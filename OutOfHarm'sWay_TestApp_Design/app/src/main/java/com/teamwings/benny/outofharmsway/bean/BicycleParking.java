package com.teamwings.benny.outofharmsway.bean;

import java.io.Serializable;

public class BicycleParking implements Serializable {

private String asset_class;
private String asset_type;
private String company;
private String condition_rating;
private String description;
private String division;
private String easting;
private String evaluation_date;
private BicycleLocation geometry;
private String gis_id;
private String location_desc;
private String model_descr;
private String model_no;
private String northing;

    public BicycleParking(String asset_class, String asset_type, String company, String condition_rating, String description, String division, String easting, String evaluation_date, BicycleLocation geometry, String gis_id, String location_desc, String model_descr, String model_no, String northing, String upload_date) {
        this.asset_class = asset_class;
        this.asset_type = asset_type;
        this.company = company;
        this.condition_rating = condition_rating;
        this.description = description;
        this.division = division;
        this.easting = easting;
        this.evaluation_date = evaluation_date;
        this.geometry = geometry;
        this.gis_id = gis_id;
        this.location_desc = location_desc;
        this.model_descr = model_descr;
        this.model_no = model_no;
        this.northing = northing;
        this.upload_date = upload_date;
    }

    private String upload_date;

    public String getAsset_class() {
        return asset_class;
    }

    public String getAsset_type() {
        return asset_type;
    }

    public String getCompany() {
        return company;
    }

    public String getCondition_rating() {
        return condition_rating;
    }

    public String getDescription() {
        return description;
    }

    public String getDivision() {
        return division;
    }

    public String getEasting() {
        return easting;
    }

    public String getEvaluation_date() {
        return evaluation_date;
    }

    public BicycleLocation getGeometry() {
        return geometry;
    }

    public String getGis_id() {
        return gis_id;
    }

    public String getLocation_desc() {
        return location_desc;
    }

    public String getModel_descr() {
        return model_descr;
    }

    public String getModel_no() {
        return model_no;
    }

    public String getNorthing() {
        return northing;
    }

    public String getUpload_date() {
        return upload_date;
    }
}

/*
{"asset_class":"Outdoor Furniture",
        "asset_type":"Bicycle Rails",
        "company":"City of Melbourne",
        "condition_rating":"2.33",
        "description":"Bicycle Rails - Galvanised Steel Bicycle Rail",
        "division":"Engineering Services Group",
        "easting":"319812.74",
        "evaluation_date":"2018-06-05T15:13:10.000",
        "geometry":{"type":"Point","coordinates":[144.9534963031,-37.794565824756]},
        "gis_id":"1073806",
        "location_desc":"Royal Park approximately 27m NW of 90 Gatehouse Street, Parkville, 3052",
        "model_descr":"Bicycle Rails - Galvanised Steel Bicycle Rail",
        "model_no":"200101-002",
        "northing":"5815005.37",
        "upload_date":"2018-09-29T02:54:49.000"}*/
