package com.teamwings.benny.outofharmsway.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.teamwings.benny.myapplication.R;
import com.teamwings.benny.outofharmsway.fragment.CrimeData;

import java.util.List;

/**
 * search
 *
 * @Date 2018-09-26.
 */
public class SearchAdapter extends BaseAdapter {
    private Context mContext;
    private List<CrimeData> list;

    public SearchAdapter(Context mContext, List<CrimeData> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = View.inflate(mContext, R.layout.item_search, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.tv_name.setText(list.get(i).getSuburb());

        double safetyRate = list.get(i).getSafetyRate();
        String rate;
        if (safetyRate > 50000) {
            rate = " 3.8 ";
            holder.tv_value.setTextColor(Color.MAGENTA);
        } else if (safetyRate > 30000 && safetyRate < 50000) {
            rate = "4.0";
            holder.tv_value.setTextColor(Color.BLUE);
        } else {
            rate = "4.5";
            holder.tv_value.setTextColor(Color.GREEN);
        }
        //holder.tv_value.setText(rate);

        return view;
    }


    static class ViewHolder {
        public TextView tv_name;
        public TextView tv_value;

        public ViewHolder(View view) {
            this.tv_name = view.findViewById(R.id.tv_name);
            this.tv_value = view.findViewById(R.id.tv_value);
        }

    }
}
