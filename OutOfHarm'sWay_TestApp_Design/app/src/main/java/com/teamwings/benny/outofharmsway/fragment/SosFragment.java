package com.teamwings.benny.outofharmsway.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.teamwings.benny.myapplication.*;
import com.teamwings.benny.outofharmsway.activity.MainActivity;
import com.teamwings.benny.outofharmsway.activity.activity_map;
import com.teamwings.benny.outofharmsway.config.Constants;
import com.teamwings.benny.outofharmsway.config.Message1;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.concurrent.TimeUnit;

/**
 * Class for the fragment activity.
 * @Date 2018-09-10.
 */
public class SosFragment extends Fragment implements View.OnClickListener {

    private Button sendTextBtn;
    private ImageButton settingsBtn;
    private Button sendSoS15;
    private Button sendSoS30;
    private Button sendSoS;
    private Button alarmBtn;
    private Button map;
    private ImageButton instructionBtn;
    private Button flashlightBtn;
    private Button stopTimer;
    private MediaPlayer ring;
    private Switch switch1;
    private static boolean play;
    private ImageButton btn;
    private Button tgBtn;
    private boolean Flash;
    private Button quickSOSBtn;
    private Button tagSpotBtn;
    private String tagLocation;
    private String mapsURL;
    private Button goToTagBtn;
    private Spinner timerSpin;
    private Boolean mLocationPermissionGranted = true;


    boolean isFlashOn;
    boolean isTimerRunning = false;
    boolean isNotifTimerRunning = false;
    boolean isClicked = false;
    private FusedLocationProviderClient mFusedLocationProviderClient;




    ToggleButton toggle;
    String loc;
    String locationURL;
    String  myString = "10101001110111011100101010";



    View view;
    TextView timerText;
    CountDownTimer countDownTimer;
    CountDownTimer notifTimer;
    private Camera camera;
    private Camera.Parameters parameters;


    int seconds, minutes;
    private static final String FORMAT = "%02d:%02d:%02d";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container,savedInstanceState);
        getActivity().setTitle("Safety");
        View view = inflater.inflate(R.layout.fragment_sos, null);
        ring = MediaPlayer.create(getContext(), R.raw.alarm);
        initView(view);
        return view;
    }

    /**
     * gets the device location. Used by sendSOSText method.
     */
    private void getDeviceParkingLocation(){
        //Log.d(getTag(),"getDeviveLocation: getting the device current location");
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());

        try{
            if(mLocationPermissionGranted){
                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){
                            //Log.d(getTag(), "onComplete: found location!");
                            Location currentLocation = (Location) task.getResult();
                            System.out.println("Your location latitude is" + currentLocation.getLatitude() + "," + "longtitude is "+ currentLocation.getLongitude());
                            double locLat = currentLocation.getLatitude();
                            double locLon = currentLocation.getLongitude();

                            loc = "Location: \nlongitude: " + locLat + " \nlatitude: " + locLon;
                            tagLocation = "http://maps.google.com/maps?q=loc:" + locLat + "," + locLon;

                            mapsURL = tagLocation;
                           //Log.d(getTag(),locationURL);
                            //moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),DEFAULT_ZOOM, "My location");
                        }
                        else {
                            //Log.d(getTag(), "onComplete: current location is null");
                            Toast.makeText(getContext(), "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }

        }catch (SecurityException e){
            //Log.e(getTag(), "getDeviceLocation: SecurityException: " + e.getMessage());
            Toast.makeText(getContext(), "unable to get current location \n"+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * method to play alarm sound
     */
    public void playStop(){




        if(!ring.isPlaying()){
            ring.start();
            alarmBtn.setBackgroundColor(Color.LTGRAY);
        }
        else if(ring.isPlaying()){
            ring.stop();
            alarmBtn.setBackgroundColor(Color.WHITE);
            ring = MediaPlayer.create(getContext(),R.raw.alarm);
        }
    }


    private void initView(final View view) {

        sendTextBtn = view.findViewById(R.id.sendTextBtn);
        sendSoS15 = view.findViewById(R.id.SOSTimerBtn2);
        sendSoS30 = view.findViewById(R.id.SOSTimerBtn3);
        timerText = view.findViewById(R.id.textView2);
        sendSoS = view.findViewById(R.id.SOSTimerBtn);
        alarmBtn = view.findViewById(R.id.alarmBtn);
        tgBtn = view.findViewById(R.id.toggleButton);
        quickSOSBtn = view.findViewById(R.id.quickSOS);
        tagSpotBtn = view.findViewById(R.id.tagLoc);
        goToTagBtn = view.findViewById(R.id.goToTag);

        map = view.findViewById(R.id.map);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent (getContext(), activity_map.class);
                startActivity(mapIntent);
            }
        });


        stopTimer = view.findViewById(R.id.StopTimerBtn);
        sendTextBtn.setOnClickListener(this);


        sendSoS15.setOnClickListener(this);
        sendSoS30.setOnClickListener(this);
        sendSoS.setOnClickListener(this);
        alarmBtn.setOnClickListener(this);
        tgBtn.setOnClickListener(this);
        quickSOSBtn.setOnClickListener(this);
        tagSpotBtn.setOnClickListener(this);
        goToTagBtn.setOnClickListener(this);


        stopTimer.setOnClickListener(this);
        sendSoS15.setTag(1);
        sendSoS15.setText("15 Minutes");
        sendSoS.setTag(1);
        sendSoS.setText("5 Minutes");
        sendSoS30.setTag(1);
        sendSoS30.setText("30 Minutes");

    }


    /**
     * Different Methods to run the flashlight
     * @param v
     */

    /*@RequiresApi(api = Build.VERSION_CODES.M)
    private void flashLightOff() {
        CameraManager cameraManager = (CameraManager) getActivity(). getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraId = cameraManager.getCameraIdList()[0];
            cameraManager.setTorchMode(cameraId, false);
        } catch (CameraAccessException e) {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void flashLightOn() {
        CameraManager cameraManager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);

        try {
            String cameraId = cameraManager.getCameraIdList()[0];
            cameraManager.setTorchMode(cameraId, true);
        } catch (CameraAccessException e) {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void flashBlink()
    {
        tgBtn.setText("Flashlight Off");
        CameraManager cameraManager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
        String myString = "0101010101";
        long blinkDelay = 50; //Delay in ms
        for (int i = 0; i < myString.length(); i++) {
            if (myString.charAt(i) == '0') {
                try {
                    String cameraId = cameraManager.getCameraIdList()[0];
                    cameraManager.setTorchMode(cameraId, true);
                } catch (CameraAccessException e) {
                }
            } else {
                try {
                    String cameraId = cameraManager.getCameraIdList()[0];
                    cameraManager.setTorchMode(cameraId, false);
                } catch (CameraAccessException e) {
                }
            }
            try {
                Thread.sleep(blinkDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
*/











    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendTextBtn:
                //getLocation();
                sendSOSText();
                break;

            case R.id.SOSTimerBtn2:
                if ((Integer) v.getTag() == 1) {
                    setTimer(15);
                    setNotifTimer(13);
                    timerHintToast();
                    sendSoS.setText("5 Minutes");
                    sendSoS30.setText("30 Minutes");
                    stopTimer.setVisibility(v.VISIBLE);
                    sendSoS15.setText("Reset");
                    v.setTag(1);
                } else {
                    sendSoS15.setText("15 Minutes");
                    countDownTimer.cancel();
                    setTimer(15);
                    v.setTag(0);
                }
                break;
            case R.id.SOSTimerBtn3:
                final int status = (Integer) v.getTag();
                if (status == 1) {
                    setTimer(30);
                    setNotifTimer(28);
                    timerHintToast();
                    sendSoS.setText("5 Minutes");
                    sendSoS15.setText("15 Minutes");
                    sendSoS30.setText("30 Minutes");
                    stopTimer.setVisibility(v.VISIBLE);
                    sendSoS30.setText("Reset");
                    v.setTag(1);
                } else {
                    countDownTimer.cancel();
                    setTimer(30);
                    v.setTag(0);
                }
                break;
            case R.id.SOSTimerBtn:
                final int status2 = (Integer) v.getTag();
                if (status2 == 1) {
                    setTimer(5);
                    setNotifTimer(3);
                    timerHintToast();
                    sendSoS15.setText("15 Minutes");
                    sendSoS30.setText("30 Minutes");
                    stopTimer.setVisibility(v.VISIBLE);
                    sendSoS.setText("Reset");
                    v.setTag(1);
                } else {
                    sendSoS.setText("5 Minutes");
                    countDownTimer.cancel();
                    setTimer(5);
                    v.setTag(0);
                }
                break;
            case R.id.alarmBtn:
                playStop();
                break;
            case R.id.map:
                Uri uri = Uri.parse("geo:37.7749,-122.4194");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
                break;

            case R.id.toggleButton:
                blinkFlash(myString);

                break;

            case R.id.quickSOS:
                sendSOSText();
                alarmBtn.callOnClick();
                break;

            case R.id.StopTimerBtn:
                countDownTimer.cancel();
                notifTimer.cancel();
                stopTimer.setVisibility(v.INVISIBLE);
                sendSoS.setText("5 Minutes");
                sendSoS15.setText("15 Minutes");
                sendSoS30.setText("30 Minutes");
                timerText.setText("00:00");
                break;

            case R.id.tagLoc:
                setTagLocation();
                break;

            case R.id.goToTag:
                String url = tagLocation;

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                //gotoBikeParking();
                goToTagBtn.setVisibility(View.INVISIBLE);
                tagSpotBtn.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * Method to get the parking location of the user on Button click
     */
    private void setTagLocation() {
        getDeviceParkingLocation();
        //getTagLocation();
        tagSpotBtn.setVisibility(View.INVISIBLE);
        goToTagBtn.setVisibility(View.VISIBLE);
    }


    /**
     * runs the flashligh in morse code for SOS
     * change the string for a different or longer sequence
     * @param myString
     */
    private void blinkFlash(String myString) {
        //myString = "10101001110111011100101010";
        long blinkDelay = 50; //Delay in ms


        try {
            camera = android.hardware.Camera.open();
        } catch (RuntimeException ex) {
        }

        for (int i = 0; i < myString.length(); i++) {
            if (myString.charAt(i) == '1') {
                parameters = camera.getParameters();
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(parameters);
                camera.startPreview();
                isFlashOn = true;
            } else {
                parameters = camera.getParameters();
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(parameters);
                camera.stopPreview();
                isFlashOn = false;

            }
            try {
                Thread.sleep(blinkDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Function to send text with a text message.
     */
    public void sendSOSText() {
        String phoneNumber, messageToSend;

        getDeviceParkingLocation();

        SharedPreferences preferences = getContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0); // 0 - for private mode
        phoneNumber = preferences.getString(Constants.EMERGENCY_PHONE_NO_SPK, Message1.DEFAULT_PREFERENCE_MESSAGE.getMessage());
        messageToSend = preferences.getString(Constants.EMERGENCY_MESSAGE_SPK, Message1.DEFAULT_PREFERENCE_MESSAGE.getMessage())+"\nLocation :"+
                "\n"+tagLocation;
        //TODO check if the information is found in the preferences. If not then prompt him to save the data
        Toast toast;
        if (messageToSend.isEmpty()) {
            toast = Toast.makeText(getContext(),
                    Message1.SOMETHING_WRONG.getMessage(),
                    Toast.LENGTH_LONG);
        } else {
            SmsManager.getDefault().sendTextMessage(phoneNumber, null, messageToSend, null, null);
            toast = Toast.makeText(getContext(),
                    Message1.SMS_SENT.getMessage(),
                    Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    /**
     * sets timer
     *
     * @param minutes duration of timer
     */
    public void setTimer(int minutes) {
        //boolean isRunning = false;

        final long timerTime = minutes * 60 *1000;
        if (isTimerRunning) {
            countDownTimer.cancel();
        }
        countDownTimer = new CountDownTimer(timerTime/*minutes * 60 * 1000*/, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                long timerCheck = timerTime;
               /* timerCheck -= 1000;
                if(millisUntilFinished == 120000)
                {
                    ((MainActivity) getActivity()).notificationCall();
                }*/

                timerText.setText("" + String.format(FORMAT,
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                ////isTimerRunning = true

            }

            public void onFinish() {
                sendSOSText();
                isTimerRunning = false;
                timerText.setText("00:00");
                Toast toast = Toast.makeText(getContext(), Message1.SMS_SENT.getMessage(), Toast.LENGTH_LONG);
                toast.show();
                //timerText.setText(Message.SMS_SENT.getMessage());
            }
        }.start();
        isTimerRunning = true;

    }

    public void timerHintToast()
    {

        Toast toast = Toast.makeText(getContext(), Message1.RESET_BUTTON_HINT.getMessage(), Toast.LENGTH_LONG);
        toast.show();
    }

    /**
     * Sets a timer along with the normal timer
     * to show a notification when 2 minutes are left.
     * @param minutes
     */
    public void setNotifTimer(int minutes)
    {
        final long timerTime = minutes * 60 *1000;
        if (isNotifTimerRunning) {
            notifTimer.cancel();
        }
        notifTimer = new CountDownTimer(timerTime/*minutes * 60 * 1000*/, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished)
            {

            }

            public void onFinish() {
                ((MainActivity) getActivity()).notificationCall();
                isNotifTimerRunning = false;
            }
        }.start();
    }

}