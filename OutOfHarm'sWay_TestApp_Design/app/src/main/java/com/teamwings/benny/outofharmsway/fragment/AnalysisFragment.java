package com.teamwings.benny.outofharmsway.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.teamwings.benny.myapplication.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @Date 2018-09-10.
 */
public class AnalysisFragment extends Fragment {

    private EditText suburbsText;
    private Button analysisBtn;
    private TextView suburbName;
    private TextView safetyRating;
    private TextView sRate;
    private List<CrimeData> crimeData= new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_analysis, null);
        suburbsText = view.findViewById(R.id.suburb);
        analysisBtn = view.findViewById(R.id.analysisBtn);
        suburbName = view.findViewById(R.id.suburbName);
        safetyRating = view.findViewById(R.id.safetyrate);
        sRate = view.findViewById(R.id.sRate);



        analysisBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getCrimeData();
                searchSuburb();
            }
        });

        readCrimeStatsData();

        return view;
    }


    public void getCrimeData() {
        String sName = suburbsText.getText().toString();
        CrimeData cData = fetchCrimeDataBySuburb(sName);
        suburbName.setText(cData.getSuburb());

    }

    public CrimeData fetchCrimeDataBySuburb(String sName) {
        InputStream is = getResources().openRawResource(R.raw.data_cleaned);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8"))
        );

        String line = "";
        int check = 0;
        String checkString = "";
        CrimeData crimeRead = null;
        try {
            reader.readLine();
            while (!(checkString.equalsIgnoreCase(sName))) {

                String[] tokens = line.split(",");

                //Read data
                crimeRead = new CrimeData();


                crimeRead.setSuburb(tokens[0]);
                //checkString = crimeRead.getSuburb();
                crimeRead.setCriminalDamage(Integer.parseInt(tokens[1]));
                crimeRead.setBicycleTheft(Integer.parseInt(tokens[2]));
                crimeRead.setDrunkNDisorderly(Integer.parseInt(tokens[3]));
                crimeRead.setDrugUse(Integer.parseInt(tokens[4]));
                crimeRead.setNonAggraBurgalary(Integer.parseInt(tokens[5]));
                crimeRead.setSafetyRate(Integer.parseInt(tokens[6]));
                check++;
                crimeData.add(crimeRead);
            }
        } catch (IOException e) {
            Log.wtf("OOHW", "Error fetching data on line " + line, e);
            e.printStackTrace();
        }
        return crimeRead;
    }

    public void searchSuburb()
    {
        String sName = suburbsText.getText().toString().toUpperCase();
            int size = crimeData.size();
            int i = 0;
            int exist = 0;
            double safetyFactor = 0.0;
            while (i < size)
            {
                if(crimeData.get(i).getSuburb().toUpperCase().equals(sName))
                {
                    suburbName.setText(crimeData.get(i).getSuburb());
                    safetyFactor = crimeData.get(i).getSafetyRate();
                    if(safetyFactor>50000) {
                        sRate.setText("3.8");
                        sRate.setTextColor(Color.MAGENTA);
                    }else if(safetyFactor>30000 && safetyFactor < 50000){
                        sRate.setText("4.0");
                        sRate.setTextColor(Color.BLUE);
                    }else{
                        sRate.setText("4.5 ");
                        sRate.setTextColor(Color.GREEN);
                    }


                    i++;
                    exist++;
                }
                else
                    i++;
            }

            if(exist == 0)
                //System.out.println("The movie does not exist.");
                suburbName.setText("Suburb not listed");
    }

    private void readCrimeStatsData() {
        InputStream is = getResources().openRawResource(R.raw.data_cleaned);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8"))
        );

        String line = "";
        int check = 0;
        try {
            reader.readLine();
            while ((line = reader.readLine()) != null) {

                String[] tokens = line.split(",");

                //Read data
                CrimeData crimeRead = new CrimeData();
                crimeRead.setSuburb(tokens[0]);
                crimeRead.setCriminalDamage(Integer.parseInt(tokens[1]));
                crimeRead.setBicycleTheft(Integer.parseInt(tokens[2]));
                crimeRead.setDrunkNDisorderly(Integer.parseInt(tokens[3]));
                crimeRead.setDrugUse(Integer.parseInt(tokens[4]));
                crimeRead.setPedestrianCrime(Integer.parseInt(tokens[5]));
                crimeRead.setNonAggraBurgalary(Integer.parseInt(tokens[6]));
                crimeRead.setSafetyRate(Double.parseDouble(tokens[7]));
                check++;
                crimeData.add(crimeRead);

                Log.d("OOHW", "Data read successful: " +crimeRead+ " row "+check);
            }
        } catch (IOException e){
            Log.wtf("OOHW", "Error fetching data on line "+line, e);
            e.printStackTrace();
        }
    }

}
