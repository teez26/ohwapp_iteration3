package com.teamwings.benny.outofharmsway.activity;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.teamwings.benny.myapplication.R;
import com.teamwings.benny.outofharmsway.SaveDataActivityFromSettings;
import com.teamwings.benny.outofharmsway.adapter.ApiFetchJson;
import com.teamwings.benny.outofharmsway.bean.BicycleLocation;
import com.teamwings.benny.outofharmsway.bean.BicycleParking;
import com.teamwings.benny.outofharmsway.fragment.AnalysisFragment2;
import com.teamwings.benny.outofharmsway.fragment.GuideFragment;
import com.teamwings.benny.outofharmsway.fragment.SosFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private RadioGroup rg_main;
    private Fragment[] fragments;
    private Fragment sosFragment, guideFragment, analysisFragment2,mapFragment;
    private int currentIndex,index;
    private Button alarmBtn;
    private static final String CAMERA_SERVICE = Manifest.permission.CAMERA;
    private static final String STORAGE_SERVICE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String PHONE_SERVICE = Manifest.permission.READ_PHONE_STATE;
    private static final String CONTACT_SERVICE = Manifest.permission.READ_CONTACTS;
    private static final String TEXT_SEND_MANAGER_SERVICE = Manifest.permission.SEND_SMS;
    private static final String TEXT_RECEIVE_MANAGER_SERVICE = Manifest.permission.RECEIVE_SMS;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 5678;
    private static final int STORAGE_PERMISSION_REQUEST_CODE = 1245;
    private static final int PHONE_PERMISSION_REQUEST_CODE = 1223;
    private static final int SMS_PERMISSION_REQUEST_CODE = 1234;
    private static final int CONTACT_PERMISSION_REQUEST_CODE = 1235;
    private Boolean mLocationPermissionGranted = false;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 9012;
    private Boolean cameraPermissionGranted = false;
    private Boolean smsPermissionGranted = false;
    private Boolean contactPermissionGranted = false;
    private Boolean phonePermissionGranted = false;
    private Boolean storagePermissionGranted = false;


    String channel_name = "default channel";
    String channel_description = "default channel for OHW app.";
    //String notificationId;
    String CHANNEL_ID;
    String notificationId = "334411";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initView();
        //buildJSONApi();
    }



    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch (item.getItemId()){
            case R.id.instruction:
                Intent insIntent = new Intent(this, activity_instruction.class);
                startActivity(insIntent);
                return true;
            case R.id.settings:
                Intent startIntent = new Intent(this, SaveDataActivityFromSettings.class);
                startActivity(startIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);


        }


    }


    private void initView() {
        rg_main = findViewById(R.id.rg_main);
        alarmBtn = findViewById(R.id.alarmBtn);
        sosFragment = new SosFragment();
        guideFragment = new GuideFragment();
        analysisFragment2 = new AnalysisFragment2();
        //Todo: add the map as a fragment. Remove commented code below in this method to add it as a fragment.
        //mapFragment = new MapFragment();


        fragments = new Fragment[]{sosFragment, guideFragment, analysisFragment2, mapFragment};
        getSupportFragmentManager().beginTransaction().add(R.id.fl_main, sosFragment)
                //.add(R.id.fl_main, mapFragment).hide(mapFragment)
                .add(R.id.fl_main, guideFragment).hide(guideFragment).show(sosFragment).commit();
        rg_main.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_shield:
                        index = 0;
                        setTitle("Safety");
                        break;
                    case R.id.rb_guide:
                        index = 1;
                        setTitle("Guidelines");
                        break;
                    case R.id.rb_analysis:
                        index = 2;
                        setTitle("Analysis");
                        break;

                    /*case R.id.rb_map:
                        index = 3;
                        setTitle("Utilities Map");
                        break;*/
                }

                showFragment(index);
            }
        });
    }

    public void isClick(View view){
        alarmBtn.setBackgroundResource(R.color.colorChecked);
    }

    public void buildJSONApi()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiFetchJson.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiFetchJson api = retrofit.create(ApiFetchJson.class);

        Call<List<BicycleParking>> call = api.getParkingData();
        call.enqueue(new Callback<List<BicycleParking>>() {
            @Override
            public void onResponse(Call<List<BicycleParking>> call, Response<List<BicycleParking>> response) {

                List<BicycleParking> bicycleParkings = response.body();

                for(BicycleParking bp : bicycleParkings)
                {
                    BicycleLocation parking = bp.getGeometry();

//                Log.d("Location", String.valueOf(bp.getGeometry()));
                    Log.d("Name", bp.getGis_id());
                    Log.d("coordinates", parking.getLatitude()+ " "+ "//"+parking.getLongitude());
                }
            }

            @Override
            public void onFailure(Call<List<BicycleParking>> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    /**
     * builds a notification to be displayed on the android system.
     */
    public void notificationCall()
    {
//        notificationId = "334411";

        createNotificationchannel();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat
                .Builder(this, CHANNEL_ID)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_shield_normal)
                .setContentTitle("SOS Timer Expiring")
                .setContentText("SOS Message will be sent in 2 Minutes")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true);


        //showing the notification

        NotificationManagerCompat notificationManger = NotificationManagerCompat.from(this);

        notificationManger.notify(Integer.parseInt(notificationId), notificationBuilder.build());

    }

    /**
     * notifications need a channel in the newer versions of the andorid OS. THis method creates a channel for that notification. The channel id specified below is used by the notification
     * of this application.
     */
    public void createNotificationchannel()
    {


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            CHANNEL_ID = "445511";
            CharSequence channel_name = "OOHW_Default";
            //CharSequence name = getString(R.string.channel_name);
            //String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    channel_name, importance);
            channel.setDescription(channel_description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }



    /**
     * initializes first fragment
     *
     * @param index：0-2
     */
    public void showFragment(int index) {
        if (currentIndex != index) {
            FragmentTransaction trx = getSupportFragmentManager().beginTransaction();
            trx.hide(fragments[currentIndex]);
            if (!fragments[index].isAdded()) {
                trx.add(R.id.fl_main, fragments[index]);
            }
            trx.show(fragments[index]).commit();
        }

        currentIndex = index;
    }
}
