package com.teamwings.benny.outofharmsway.util;

import com.teamwings.benny.outofharmsway.adapter.ApiFetchJson;
import com.teamwings.benny.outofharmsway.bean.BicycleParking;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Load Parking Data
 */
public class LoadParkingDataUtils {


    /**
     * load parking json data
     *
     * @param callback
     */
    public static void loadParkingJson(final LoadParkingCallback callback){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiFetchJson.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiFetchJson api = retrofit.create(ApiFetchJson.class);

        Call<List<BicycleParking>> call = api.getParkingData();
        call.enqueue(new Callback<List<BicycleParking>>() {
            @Override
            public void onResponse(Call<List<BicycleParking>> call, Response<List<BicycleParking>> response) {
                List<BicycleParking> bicycleParkings = response.body();
                if(callback != null){
                    callback.loadSuccess(bicycleParkings);
                }
            }

            @Override
            public void onFailure(Call<List<BicycleParking>> call, Throwable t) {
                if(callback != null){
                    callback.loadFailed(t.getMessage());
                }
            }
        });

    }
}
