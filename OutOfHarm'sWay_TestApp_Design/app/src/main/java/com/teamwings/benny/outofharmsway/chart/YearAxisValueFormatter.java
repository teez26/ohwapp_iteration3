package com.teamwings.benny.outofharmsway.chart;

import android.util.Log;

import com.teamwings.benny.outofharmsway.bean.SuburbDetail;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.List;

/**
 * @Date 2018-09-26.
 */
public class YearAxisValueFormatter implements IAxisValueFormatter {
    private List<SuburbDetail> list;

    public YearAxisValueFormatter(List<SuburbDetail> list) {
        this.list = list;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        Log.e("YearAxisValueFormatter", "value:" + value);
        Log.e("YearAxisValueFormatter", "list.size():" + list.size());
        if ((int) value - 1 >= list.size()) {
            return "越界"+list.size();
        }else{
            return list.get((int) value - 1).getYear();
        }

    }
}
