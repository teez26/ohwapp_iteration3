package com.teamwings.benny.outofharmsway.util;

import android.content.Context;
import android.util.Log;

import com.teamwings.benny.myapplication.R;
import com.teamwings.benny.outofharmsway.bean.SuburbDetail;
import com.teamwings.benny.outofharmsway.fragment.CrimeData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Date 2018-09-26.
 */
public class CommUtil {

    public static List<SuburbDetail> getSuburbList(Context context, String suburb, String group) {
        Log.d("CommUtil", suburb);
        Log.d("CommUtil", group);
        List<SuburbDetail> list = new ArrayList<>();
        InputStream is = context.getResources().openRawResource(R.raw.suburb_detail);
        InputStreamReader inputStreamReader = new InputStreamReader(is, Charset.forName("UTF-8"));
        BufferedReader reader = new BufferedReader(inputStreamReader);

        String line = "";
        try {
            reader.readLine();
            while ((line = reader.readLine()) != null) {

                String[] tokens = line.split(",");
                if (!(tokens[2].equalsIgnoreCase(suburb.trim()) && tokens[3].equalsIgnoreCase(group))) {
                    continue;
                }
                //Read data
                SuburbDetail detail = new SuburbDetail();
                detail.setYear(tokens[0]);
                detail.setPostcode(Integer.valueOf(tokens[1]));
                detail.setTownName(tokens[2]);
                detail.setSubGroup(tokens[3]);
                detail.setRecorded(Integer.valueOf(tokens[4].trim()));
                list.add(detail);
            }
        } catch (Exception e) {
            Log.d("CommUtil", "exception:" + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                reader.close();
                inputStreamReader.close();
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Collections.sort(list, new Comparator<SuburbDetail>() {
            @Override
            public int compare(SuburbDetail suburbDetail, SuburbDetail t1) {
                int i = suburbDetail.getYear().compareTo(t1.getYear());
                System.out.println("result" + i);
                return i > 0 ? 1 : -1;
            }
        });
        return list;
    }


    private static List<CrimeData> readCrimeStatsData(Context context, String suburb, String group) {
        List<CrimeData> crimeData = new ArrayList<>();
        InputStream is = context.getResources().openRawResource(R.raw.data_cleaned);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8"))
        );

        String line = "";
        int check = 0;
        try {
            reader.readLine();
            while ((line = reader.readLine()) != null) {

                String[] tokens = line.split(",");

                //Read data
                CrimeData crimeRead = new CrimeData();
                crimeRead.setSuburb(tokens[0]);
                crimeRead.setCriminalDamage(Integer.parseInt(tokens[1]));
                crimeRead.setBicycleTheft(Integer.parseInt(tokens[2]));
                crimeRead.setDrunkNDisorderly(Integer.parseInt(tokens[3]));
                crimeRead.setDrugUse(Integer.parseInt(tokens[4]));
                crimeRead.setPedestrianCrime(Integer.parseInt(tokens[5]));
                crimeRead.setNonAggraBurgalary(Integer.parseInt(tokens[6]));
                crimeRead.setSafetyRate(Double.parseDouble(tokens[7]));
                check++;
                crimeData.add(crimeRead);

                Log.d("OOHW", "Data read successful: " + crimeRead + " row " + check);
            }
        } catch (IOException e) {
            Log.wtf("OOHW", "Error fetching data on line " + line, e);
            e.printStackTrace();
        }
        return crimeData;
    }

    public static double calDistance(double latitude1, double longitude1, double latitude2, double longitude2) {
        double Lat1 = rad(latitude1);
        double Lat2 = rad(latitude2);
        double a = Lat1 - Lat2;
        double b = rad(longitude1) - rad(longitude2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
                + Math.cos(Lat1) * Math.cos(Lat2) * Math.pow(Math.sin(b / 2), 2)));
        s = s * 6378137.0;
        s = Math.round(s * 10000d) / 10000d;
        return s;
    }

    private static double rad(double d) {
        return d * Math.PI / 180.00;
    }
}
