package com.teamwings.benny.outofharmsway;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.teamwings.benny.myapplication.R;
import com.teamwings.benny.outofharmsway.activity.MainActivity;
import com.teamwings.benny.outofharmsway.config.Constants;
import com.teamwings.benny.outofharmsway.config.Message1;


//import org.apache.commons.lang3.StringUtils;

import static android.content.SharedPreferences.Editor;
//import static java.security.AccessController.getContext;

/**
 * Settings page. Saves new data and displays old saved message and number.
 */
public class SaveDataActivityFromSettings extends AppCompatActivity {


    private String numberSelected = null;
    private String contactName = null;
    private Button contactButton;
    private Button submitBtn;
    private EditText editTextMessage;
    private TextView contactLabel;
    private String messageToSend;
    private String customMessage = "";
    private RadioButton rbMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_datapostinitiallaunch);

        contactButton = (Button) findViewById(R.id.contactButton);
        contactLabel = findViewById(R.id.contactLabel);
        rbMessage = findViewById(R.id.radio_3);
        final String text;
        final Intent intent = new Intent(this, MainActivity.class);
        loadLabelsWithSavedDetails();
        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, Constants.PICK_CONTACT_REQUEST_CODE);
            }
        });
        submitBtn = (Button) findViewById(R.id.submit);
        editTextMessage = findViewById(R.id.editTextMessage);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customMessage = editTextMessage.getText().toString();
                if (!(messageToSend.isEmpty() && customMessage.isEmpty() && !numberSelected.isEmpty()) ){
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0); // 0 - for private mode
                    Editor editor = pref.edit();
                    editor.putString(Constants.EMERGENCY_PHONE_NO_SPK, numberSelected);
                    editor.putString(Constants.EMERGENCY_CONTACT_SPK, contactName);
                    if (!customMessage.equals("")) {
                        editor.putString(Constants.EMERGENCY_MESSAGE_SPK, editTextMessage.getText().toString());
                    }else{
                        editor.putString(Constants.EMERGENCY_MESSAGE_SPK, messageToSend);
                    }
                    editor.apply();

                    finish();
                    //PendingIntent startIntent =  PendingIntent.getActivity(getApplicationContext(),1,intent,PendingIntent.FLAG_UPDATE_CURRENT);

                }
                else if (editTextMessage.getText().toString().equals("") && !numberSelected.equals("")){
                    /*if (editTextMessage.getText().toString() == "Null") {*/
                    Toast toast = Toast.makeText(getApplicationContext(),
                            Message1.BLANK_MESSAGE.getMessage(),
                            Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            Message1.CONTACT_UNSELECTED.getMessage(),
                            Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });



    }

    /**
     * Loads the labels on the activity with previously saved data
     */
    private void loadLabelsWithSavedDetails() {
        String phoneNumber, name;
        EditText editTextMessageonCreate;
        SharedPreferences preferences = getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0);
        /*SharedPreferences preferences = AccessController.getContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0); // 0 - for private mode*/
        phoneNumber = preferences.getString(Constants.EMERGENCY_PHONE_NO_SPK, Message1.DEFAULT_PREFERENCE_MESSAGE.getMessage());
        contactName = preferences.getString(Constants.EMERGENCY_CONTACT_SPK, Message1.DEFAULT_PREFERENCE_MESSAGE.getMessage());
        messageToSend = preferences.getString(Constants.EMERGENCY_MESSAGE_SPK, Message1.DEFAULT_PREFERENCE_MESSAGE.getMessage());

        if (!messageToSend.isEmpty())
        {
            rbMessage.setChecked(true);
            editTextMessageonCreate = findViewById(R.id.editTextMessage);
            editTextMessageonCreate.setVisibility(View.VISIBLE);

            editTextMessageonCreate.setText(messageToSend);
        }
        else
            editTextMessage.setText("");

        if (phoneNumber.isEmpty())
        {
            /*contactLabel.setText(Constants.EMERGENCY_PHONE_NO_SPK);*/
            contactLabel.setText("No Contact Selected");
        }
        else
        {
            contactLabel.setText(contactName+": "+phoneNumber);
            numberSelected = phoneNumber;
        }
    }

    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0);
        Editor editor = pref.edit();



        switch(view.getId()) {
            case R.id.radio_1:
                if (checked)
                editTextMessage.setVisibility(View.INVISIBLE);
                editTextMessage.setText("");
                messageToSend = "Please help me.";
                break;
            case R.id.radio_2:
                if (checked)
                    editTextMessage.setVisibility(View.INVISIBLE);
                    editTextMessage.setText("");
                    messageToSend = "Please send help. I am in an emergency.";
                    break;
            case R.id.radio_3:
                if (checked)
                    editTextMessage.setVisibility(View.VISIBLE);
                    break;
        }
    }


    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (Constants.PICK_CONTACT_REQUEST_CODE):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone =
                                c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                            phones.moveToFirst();
                            numberSelected = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            contactName =  phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                            contactLabel.setText(contactName+": "+numberSelected);
                            // Toast.makeText(getApplicationContext(), cNumber, Toast.LENGTH_SHORT).show();
                            // setCn(cNumber);
                        }
                    }
                }
        }

    }


}
