package com.teamwings.benny.outofharmsway.bean;

import java.io.Serializable;

/**
 * @Date 2018-09-26.
 */
public class SuburbDetail implements Serializable {
    private String year;
    private int postcode;
    private String townName;
    private String subGroup;
    private int recorded;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(String townName) {
        this.townName = townName;
    }

    public String getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(String subGroup) {
        this.subGroup = subGroup;
    }

    public int getRecorded() {
        return recorded;
    }

    public void setRecorded(int recorded) {
        this.recorded = recorded;
    }
}
