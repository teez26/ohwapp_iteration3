package com.teamwings.benny.outofharmsway.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.teamwings.benny.myapplication.R;
import com.teamwings.benny.outofharmsway.activity.ChartActivity;
import com.teamwings.benny.outofharmsway.adapter.SearchAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Copy of AnalysisFragment
 *
 * @Date 2018-09-26.
 */
public class AnalysisFragment2 extends Fragment {

    private List<CrimeData> crimeData = new ArrayList<>();
    private EditText et_search;
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getActivity().setTitle("Analysis");
        View view = inflater.inflate(R.layout.fragment_analysis2, null);
        initView(view);

        readCrimeStatsData();

        initListView();

        return view;
    }

    private void initListView() {
        listView.setAdapter(new SearchAdapter(getContext(), crimeData));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getContext(), ChartActivity.class);
                intent.putExtra("data", crimeData.get(i));
                startActivity(intent);
            }
        });
    }

    private void initView(View view) {
        et_search = (EditText) view.findViewById(R.id.et_search);
        listView = (ListView) view.findViewById(R.id.listView);
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Log.d("AnalysisFragment2", "editable:" + editable);
                String search = editable.toString().toUpperCase();
//                String search = et_search.getText().toString().trim().toUpperCase();
                if (TextUtils.isEmpty(search)) {
                    readCrimeStatsData();
                } else {
                    readCrimeStatsData();
                    filterData(search);
                }
                initListView();
            }
        });

    }

    private void filterData(String search) {
        List<CrimeData> temp = new ArrayList<>();

        for (CrimeData cd : crimeData) {
            if (cd.getSuburb().startsWith(search)) {
                temp.add(cd);
            }
        }
        crimeData = temp;
    }

    private void readCrimeStatsData() {
        crimeData.clear();
        InputStream is = getResources().openRawResource(R.raw.data_cleaned);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, Charset.forName("UTF-8"))
        );

        String line = "";
        int check = 0;
        try {
            reader.readLine();
            while ((line = reader.readLine()) != null) {

                String[] tokens = line.split(",");

                //Read data
                CrimeData crimeRead = new CrimeData();
                crimeRead.setSuburb(tokens[0]);
                crimeRead.setCriminalDamage(Integer.parseInt(tokens[1]));
                crimeRead.setBicycleTheft(Integer.parseInt(tokens[2]));
                crimeRead.setDrunkNDisorderly(Integer.parseInt(tokens[3]));
                crimeRead.setDrugUse(Integer.parseInt(tokens[4]));
                crimeRead.setPedestrianCrime(Integer.parseInt(tokens[5]));
                crimeRead.setNonAggraBurgalary(Integer.parseInt(tokens[6]));
                crimeRead.setSafetyRate(Double.parseDouble(tokens[7]));
                check++;
                crimeData.add(crimeRead);

                //Log.d("OOHW", "Data read successful: " + crimeRead + " row " + check);
            }
        } catch (IOException e) {
            //Log.wtf("OOHW", "Error fetching data on line " + line, e);
            e.printStackTrace();
        }finally {
            try {
                reader.close();
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
