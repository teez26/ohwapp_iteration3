package com.teamwings.benny.outofharmsway.util;

import com.teamwings.benny.outofharmsway.bean.BicycleParking;

import java.util.List;

/**
 * load parking interface
 */
public interface LoadParkingCallback {

    void loadSuccess(List<BicycleParking> list);

    void loadFailed(String errorMsg);
}
