package com.teamwings.benny.outofharmsway.adapter;

import com.teamwings.benny.outofharmsway.bean.BicycleParking;
import com.teamwings.benny.outofharmsway.bean.ToiletLocation;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiFetchJson {
    String BASE_URL = "https://data.melbourne.vic.gov.au/resource/";

    @GET("w4fc-iq27.json")
    Call<List<BicycleParking>> getParkingData();

    @GET("dsec-5y6t.json")
    Call<List<ToiletLocation>> getToiletData();
}
