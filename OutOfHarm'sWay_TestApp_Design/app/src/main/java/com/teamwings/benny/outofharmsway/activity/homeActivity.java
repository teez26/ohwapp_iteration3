package com.teamwings.benny.outofharmsway.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import static android.content.SharedPreferences.*;
import android.os.Handler;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.teamwings.benny.myapplication.R;
import com.teamwings.benny.outofharmsway.config.Constants;

public class homeActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        int layoutResId = -1;
        final Class targetActivity[] = new Class[1];
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0); // 0 - for private mode
        Editor editor = pref.edit();
        if(pref.getBoolean(Constants.FIRST_TIME_SPK,true)){
            layoutResId = R.layout.activity_save_data;
            targetActivity[0] = SaveDataActivity.class;
            editor.putBoolean(Constants.FIRST_TIME_SPK, false);
            editor.commit();
        }
        else{
            layoutResId = R.layout.activity_home;
            targetActivity[0] = MainActivity.class;
        }

        setContentView(layoutResId);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent homeIntent = new Intent(homeActivity.this, targetActivity[0]);
                startActivity(homeIntent);
                finish();
            }
        }, Constants.SPLASH_DURATION);
    }
}
