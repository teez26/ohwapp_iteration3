package com.teamwings.benny.outofharmsway.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.teamwings.benny.myapplication.R;
import com.teamwings.benny.outofharmsway.bean.SuburbDetail;
import com.teamwings.benny.outofharmsway.chart.MyAxisValueFormatter;
import com.teamwings.benny.outofharmsway.chart.YearAxisValueFormatter;
import com.teamwings.benny.outofharmsway.fragment.CrimeData;
import com.teamwings.benny.outofharmsway.util.CommUtil;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;
import java.util.List;

public class ChartActivity extends AppCompatActivity implements OnChartValueSelectedListener {

    private List<SuburbDetail> list;

    protected BarChart mChart;
    private Spinner spinner01;

    protected Typeface mTfRegular;
    protected Typeface mTfLight;

    private RadarChart radarChart;

    private CrimeData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        init();


        initChart();


        initLeiDa();


    }

    private void initLeiDa() {
        radarChart = findViewById(R.id.chart2);
        radarChart.setBackgroundColor(Color.rgb(60, 65, 82));

        radarChart.getDescription().setEnabled(false);

        radarChart.setWebLineWidth(1f);
        radarChart.setWebColor(Color.LTGRAY);
        radarChart.setWebLineWidthInner(1f);
        radarChart.setWebColorInner(Color.LTGRAY);
        radarChart.setWebAlpha(100);

        setLeiDaData();

        XAxis xAxis = radarChart.getXAxis();
        xAxis.setTypeface(mTfLight);
        xAxis.setTextSize(9f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            //            private String[] mActivities = new String[]{"Criminal damage", "Theft of a bicycle", "Drunk and disorderly in public", "Drug use", "Non-residential aggravated burglary"};
            private String[] mActivities = new String[]{"Cdamage", "Theft", "Drunk", "Drug", "Non-re"};

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return mActivities[(int) value % mActivities.length];
            }
        });
        xAxis.setTextColor(Color.WHITE);

        YAxis yAxis = radarChart.getYAxis();
        yAxis.setTypeface(mTfLight);
        yAxis.setLabelCount(5, false);
        yAxis.setTextSize(9f);
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(80f);
        yAxis.setDrawLabels(false);

        Legend l = radarChart.getLegend();
        l.setEnabled(false);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setTypeface(mTfLight);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        l.setTextColor(Color.WHITE);
    }

    private void init() {
        data = (CrimeData) getIntent().getSerializableExtra("data");
        setTitle(data.getSuburb());


        mTfRegular = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
        mTfLight = Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf");

        spinner01 = findViewById(R.id.Spinner01);
        spinner01.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                list = CommUtil.getSuburbList(ChartActivity.this, data.getSuburb(), (String) spinner01.getSelectedItem());
                mChart.clearValues();
//                Toast.makeText(ChartActivity.this, "list.size():" + list.size(), Toast.LENGTH_SHORT).show();
                if (list.size()==0){
                    Toast.makeText(ChartActivity.this, "no data", Toast.LENGTH_SHORT).show();
                    return;
                }

                XAxis xAxis = mChart.getXAxis();
                xAxis.setLabelCount(list.size());
                setData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mChart = findViewById(R.id.chart1);
        mChart.setOnChartValueSelectedListener(this);
        list = CommUtil.getSuburbList(this, data.getSuburb(), (String) spinner01.getSelectedItem());

    }

    private void initChart() {

//        Toast.makeText(this, "list.size():" + list.size(), Toast.LENGTH_SHORT).show();


        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);
        mChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be drawn
        mChart.setMaxVisibleValueCount(60);
        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);
        mChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);
//        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
//        xAxis.setLabelCount(6);
//        xAxis.setValueFormatter(xAxisFormatter);
        xAxis.setValueFormatter(new YearAxisValueFormatter(list));

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)


        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);
//        rightAxis.setDrawGridLines(false);
//        rightAxis.setTypeface(mTfLight);
//        rightAxis.setLabelCount(8, false);
//        rightAxis.setValueFormatter(custom);
//        rightAxis.setSpaceTop(15f);
//        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = mChart.getLegend();
        l.setEnabled(false);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);


       /* XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart*/

        setData();
    }

    private void setData() {

        XAxis xAxis = mChart.getXAxis();
        xAxis.setValueFormatter(new YearAxisValueFormatter(list));

        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            Log.d("ChartActivity", "list.get(i).getRecorded():" + list.get(i).getRecorded());
            yVals1.add(new BarEntry(i + 1, list.get(i).getRecorded()));
        }


        BarDataSet set1;

        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, (String) spinner01.getSelectedItem());

            set1.setDrawIcons(false);

//            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTypeface(mTfLight);
            data.setBarWidth(0.9f);

            mChart.setData(data);
        }
    }

    protected RectF mOnValueSelectedRectF = new RectF();

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;

        RectF bounds = mOnValueSelectedRectF;
        mChart.getBarBounds((BarEntry) e, bounds);
        MPPointF position = mChart.getPosition(e, YAxis.AxisDependency.LEFT);

        Log.i("bounds", bounds.toString());
        Log.i("position", position.toString());

        Log.i("x-index",
                "low: " + mChart.getLowestVisibleX() + ", high: "
                        + mChart.getHighestVisibleX());

        MPPointF.recycleInstance(position);
    }

    @Override
    public void onNothingSelected() {

    }

    public void goToLeiDa(View view) {
        startActivity(new Intent(this, RadarActivity.class));
    }


    public void setLeiDaData() {

        float mult = 80;
        float min = 20;
        int cnt = 5;

        ArrayList<RadarEntry> entries1 = new ArrayList<RadarEntry>();
        ArrayList<RadarEntry> entries2 = new ArrayList<RadarEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < cnt; i++) {
            float val1 = (float) (Math.random() * mult) + min;
            entries1.add(new RadarEntry(val1));

//            float val2 = (float) (Math.random() * mult) + min;
//            entries2.add(new RadarEntry(val2));
        }

        int d1 = data.getCriminalDamage();
        int d2 = data.getBicycleTheft();
        int d3 = data.getDrunkNDisorderly();
        int d4 = data.getDrugUse();
        int d5 = data.getNonAggraBurgalary();
        if (d1 >= 0 && d1 < 1000) {
            d1 = 5;
        } else if (d1 >= 1000 && d1 < 2000) {
            d1 = 4;
        } else if (d1 >= 2000 && d1 < 3000) {
            d1 = 3;
        } else if (d1 >= 3000 && d1 < 4000) {
            d1 = 2;
        } else {
            d1 = 1;
        }

        if (d2 >= 0 && d2 < 100) {
            d2 = 5;
        } else if (d2 >= 100 && d2 < 200) {
            d2 = 4;
        } else if (d2 >= 200 && d2 < 500) {
            d2 = 3;
        } else if (d2 >= 500 && d2 < 1000) {
            d2 = 2;
        } else {
            d2 = 1;
        }

        if (d3 >= 0 && d3 < 200) {
            d3 = 5;
        } else if (d3 >= 200 && d3 < 400) {
            d3 = 4;
        } else if (d3 >= 400 && d3 < 600) {
            d3 = 3;
        } else if (d3 >= 600 && d3 < 800) {
            d3 = 2;
        } else {
            d3 = 1;
        }

        if (d4 >= 0 && d4 < 30) {
            d4 = 5;
        } else if (d4 >= 30 && d4 < 60) {
            d4 = 4;
        } else if (d4 >= 60 && d4 < 90) {
            d4 = 3;
        } else if (d4 >= 90 && d4 < 120) {
            d4 = 2;
        } else {
            d4 = 1;
        }

        if (d5 >= 0 && d5 < 30) {
            d5 = 5;
        } else if (d5 >= 30 && d5 < 60) {
            d5 = 4;
        } else if (d5 >= 60 && d5 < 90) {
            d5 = 3;
        } else if (d5 >= 90 && d5 < 120) {
            d5 = 2;
        } else {
            d5 = 1;
        }

        entries2.add(new RadarEntry(d1*20));
        entries2.add(new RadarEntry(d2*20));
        entries2.add(new RadarEntry(d3*20));
        entries2.add(new RadarEntry(d4*20));
        entries2.add(new RadarEntry(d5*20));



        RadarDataSet set2 = new RadarDataSet(entries2, "This Week");
        set2.setColor(Color.rgb(121, 162, 175));
        set2.setFillColor(Color.rgb(121, 162, 175));
        set2.setDrawFilled(true);
        set2.setFillAlpha(180);
        set2.setLineWidth(2f);
        set2.setDrawHighlightCircleEnabled(true);
        set2.setDrawHighlightIndicators(false);

        ArrayList<IRadarDataSet> sets = new ArrayList<IRadarDataSet>();

        sets.add(set2);

        RadarData data = new RadarData(sets);
        data.setValueTypeface(mTfLight);
        data.setValueTextSize(8f);
        data.setDrawValues(false);
        data.setValueTextColor(Color.WHITE);

        radarChart.setData(data);
        radarChart.invalidate();
    }
}
