package com.teamwings.benny.outofharmsway.bean;

public class BicycleLocation {

     private String type;
     private String coordinates[];
    private String longitude;
    private String latitude;

    public BicycleLocation(String[] coordinates) {
        this.coordinates = coordinates;
    }

    public BicycleLocation(String type, String coordinates) {
        this.type = type;
        //this.coordinates = Double.toString(Double.parseDouble(coordinates));

    }

    public String getType() {
        return type;
    }

    public String[] getCoordinates() {

        return coordinates;
    }

    public String getLongitude()
    {
        longitude = coordinates[0];
        return longitude;
    }

    public String getLatitude()
    {
        latitude = coordinates[1];
        return latitude;
    }
}

/*
geometry":{"type":"Point","coordinates":[144.965837525891,-37.80496682453]},*/
