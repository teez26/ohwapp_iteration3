package com.teamwings.benny.outofharmsway.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.teamwings.benny.outofharmsway.fragment.Tute1Fragment;
import com.teamwings.benny.outofharmsway.fragment.Tute2Fragment;
import com.teamwings.benny.outofharmsway.fragment.Tute3Fragment;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    public final int COUNT = 3;
    private String[] titles = new String[]{"Regulation", "FAQs", "Tips for Fines"};
    private Context context;
    private Fragment[] fragments;

    public MyFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        fragments = new Fragment[]{new Tute1Fragment(), new Tute2Fragment(), new Tute3Fragment()};
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
