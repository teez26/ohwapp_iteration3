package com.teamwings.benny.outofharmsway.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.teamwings.benny.myapplication.R;

/**
 * @Date 2018-09-10.
 */
public class Tute3Fragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("");
        return inflater.inflate(R.layout.activity_tutorial2, null);
    }
}