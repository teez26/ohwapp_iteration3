package com.teamwings.benny.outofharmsway.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import static android.content.SharedPreferences.*;

import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.teamwings.benny.myapplication.R;
import com.teamwings.benny.outofharmsway.config.Constants;
import com.teamwings.benny.outofharmsway.config.Message1;

/**
 * This page is only run when the app is launched for the first time.
 * TODO: Add a method to ask for permission if not given when the app is opened at a later stage as well.
 */

public class SaveDataActivity extends AppCompatActivity {


    private String numberSelected = null;
    private String contactName = null;
    private Button contactButton;
    private EditText editTextMessage;
    private Button submitButton;
    private static final String CAMERA_SERVICE = Manifest.permission.CAMERA;
    private static final String STORAGE_SERVICE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String PHONE_SERVICE = Manifest.permission.READ_PHONE_STATE;
    private static final String CONTACT_SERVICE = Manifest.permission.READ_CONTACTS;
    private static final String TEXT_SEND_MANAGER_SERVICE = Manifest.permission.SEND_SMS;
    private static final String TEXT_RECEIVE_MANAGER_SERVICE = Manifest.permission.RECEIVE_SMS;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 5678;
    private static final int STORAGE_PERMISSION_REQUEST_CODE = 1245;
    private static final int PHONE_PERMISSION_REQUEST_CODE = 1223;
    private static final int SMS_PERMISSION_REQUEST_CODE = 1234;
    private static final int CONTACT_PERMISSION_REQUEST_CODE = 1235;
    private Boolean mLocationPermissionGranted = false;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 9012;
    private Boolean cameraPermissionGranted = false;
    private Boolean smsPermissionGranted = false;
    private Boolean contactPermissionGranted = false;
    private Boolean phonePermissionGranted = false;
    private Boolean storagePermissionGranted = false;
    private TextView smsNote;
    private TextView contactLabel;
    private TextView suggestionLabel;
    private String messageToSend = "Please help me, I'm in an emergency.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_data);
        checkPermission();
        initView();
    }
    private void initView(){
        contactButton = (Button)findViewById(R.id.contactButton);
        editTextMessage = findViewById(R.id.editTextMessage);
        submitButton = findViewById(R.id.submit);
        smsNote = findViewById(R.id.smsNote);
        contactLabel = findViewById(R.id.contactLabel);
        suggestionLabel = findViewById(R.id.suggestionLabel);


        contactButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, Constants.PICK_CONTACT_REQUEST_CODE);
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(numberSelected!=null){
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0); // 0 - for private mode
                    Editor editor = pref.edit();
                    editor.putString(Constants.EMERGENCY_PHONE_NO_SPK, numberSelected);
                    editor.putString(Constants.EMERGENCY_MESSAGE_SPK, messageToSend);
                    editor.putString(Constants.EMERGENCY_CONTACT_SPK, contactName);
                    editor.commit();
                    Intent startIntent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(startIntent);
                }else{
                    Toast toast = Toast.makeText(getApplicationContext(),
                            Message1.CONTACT_UNSELECTED.getMessage(),
                            Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });

    }

    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0);
        Editor editor = pref.edit();



        switch(view.getId()) {
            case R.id.radio_1:
                if (checked)
                messageToSend = "Please help me.";
                break;
            case R.id.radio_2:
                if (checked)
                messageToSend = "Please send help. I am in an emergency.";
                break;

        }
    }

    private void checkPermission(){
        String[] permission = {Manifest.permission.CAMERA, Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this.getApplicationContext(), CAMERA_SERVICE) == PackageManager.PERMISSION_GRANTED) {
                    if(ContextCompat.checkSelfPermission(this.getApplicationContext(),CONTACT_SERVICE) == PackageManager.PERMISSION_GRANTED){
                        if(ContextCompat.checkSelfPermission(this.getApplicationContext(), TEXT_SEND_MANAGER_SERVICE) == PackageManager.PERMISSION_GRANTED){
                            if(ContextCompat.checkSelfPermission(this.getApplicationContext(), TEXT_RECEIVE_MANAGER_SERVICE) == PackageManager.PERMISSION_GRANTED){
                                if(ContextCompat.checkSelfPermission(this.getApplicationContext(), PHONE_SERVICE) == PackageManager.PERMISSION_GRANTED) {
                                    if(ContextCompat.checkSelfPermission(this.getApplicationContext(), STORAGE_SERVICE) == PackageManager.PERMISSION_GRANTED) {
                                        cameraPermissionGranted = true;
                                        contactPermissionGranted = true;
                                        mLocationPermissionGranted = true;
                                        smsPermissionGranted = true;
                                        phonePermissionGranted = true;
                                        storagePermissionGranted = true;
                                        initView();

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            ActivityCompat.requestPermissions(this, permission,STORAGE_PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(this, permission,CAMERA_PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(this, permission,CONTACT_PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(this, permission,LOCATION_PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(this, permission,SMS_PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(this, permission,PHONE_PERMISSION_REQUEST_CODE);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        cameraPermissionGranted = false;
        smsPermissionGranted = false;
        mLocationPermissionGranted = false;
        contactPermissionGranted = false;
        phonePermissionGranted = false;
        storagePermissionGranted = false;

        switch (requestCode){
            case CAMERA_PERMISSION_REQUEST_CODE:{
                if (grantResults.length > 0 ){
                    for (int i = 0; i < grantResults.length; i++){
                        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            cameraPermissionGranted = false;
                            return;
                        }
                    }
                    cameraPermissionGranted = true;
                    initView();
                }
            }
            case SMS_PERMISSION_REQUEST_CODE:{
                if (grantResults.length > 0 ){
                    for (int i = 0; i < grantResults.length; i++){
                        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            smsPermissionGranted = false;
                            return;
                        }
                    }
                    smsPermissionGranted = true;
                    initView();
                }
            }
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if (grantResults.length > 0 ){
                    for (int i = 0; i < grantResults.length; i++){
                        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionGranted = false;
                            return;
                        }
                    }
                    mLocationPermissionGranted = true;
                    initView();
                }
            }
            case CONTACT_PERMISSION_REQUEST_CODE:{
                if (grantResults.length > 0 ){
                    for (int i = 0; i < grantResults.length; i++){
                        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            contactPermissionGranted = false;
                            return;
                        }
                    }
                    contactPermissionGranted = true;
                    initView();
                }
            }
            case PHONE_PERMISSION_REQUEST_CODE:{
                if (grantResults.length > 0 ){
                    for (int i = 0; i < grantResults.length; i++){
                        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            phonePermissionGranted = false;
                            return;
                        }
                    }
                    phonePermissionGranted = true;
                    initView();
                }
            }
            case STORAGE_PERMISSION_REQUEST_CODE:{
                if (grantResults.length > 0 ){
                    for (int i = 0; i < grantResults.length; i++){
                        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            storagePermissionGranted = false;
                            return;
                        }
                    }
                    storagePermissionGranted = true;
                    initView();
                }
            }
        }
    }

    /**
     * Uses Contact API and readcontacts permission to directly pick contacts from the user's phone directory
     * @param reqCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data){
        super.onActivityResult(reqCode, resultCode, data);
        switch(reqCode)
        {
            case (Constants.PICK_CONTACT_REQUEST_CODE):
                if (resultCode == Activity.RESULT_OK)
                {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst())
                    {
                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone =
                                c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1"))
                        {
                            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null);
                            phones.moveToFirst();
                            numberSelected = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            contactName =  phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                            // Toast.makeText(getApplicationContext(), cNumber, Toast.LENGTH_SHORT).show();
                            // setCn(cNumber);
                            contactLabel.setText(contactName+": "+numberSelected);
                        }
                    }
                }
        }

    }













}
