package com.teamwings.benny.outofharmsway.config;

/**
 * Stores messages which are used throughout the app. Change them here in one place. Add any new such message here for future updates.
 */
public enum Message1 {
    SMS_SENT("Emergency Message Sent"),
    CONTACT_UNSELECTED("No Contact selected. Please select a contact to proceed"),
    DEFAULT_PREFERENCE_MESSAGE("EMPTY STRING"),
    BLANK_MESSAGE("Please enter a valid SOS Message"),
    TIMER_EXPIRE_NOTIFICATION_TITLE("2 Minutes Left"),
    TIMER_EXPIRE_NOTIFICATION_CONTENT("SOS Message will be sent in 2 Minutes"),
    RESET_BUTTON_HINT("An SMS with your location will be sent to your emergency contact at the end of the timer."),
    SOMETHING_WRONG("Something wrong! Please Select Emergency Contact and Emergency Message again.");

    private String message;


    Message1(String message){
        this.message= message;
    }
    public String getMessage(){
        return message;
    }

    public void setMessage(String message){
        this.message = message;
    }
}
