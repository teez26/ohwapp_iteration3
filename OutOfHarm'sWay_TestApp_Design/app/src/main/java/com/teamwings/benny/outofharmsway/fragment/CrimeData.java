package com.teamwings.benny.outofharmsway.fragment;


import java.io.Serializable;

public class CrimeData implements Serializable{

    private String suburb;
    private int criminalDamage;
    private int bicycleTheft;
    private int drunkNDisorderly;
    private int drugUse;
    private int nonAggraBurgalary;
    private double safetyRate;
    private int pedestrianCrime;

    public int getPedestrianCrime() {
        return pedestrianCrime;
    }

    public void setPedestrianCrime(int pedestrianCrime) {
        this.pedestrianCrime = pedestrianCrime;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public int getCriminalDamage() {
        return criminalDamage;
    }

    public void setCriminalDamage(int criminalDamage) {
        this.criminalDamage = criminalDamage;
    }

    public int getBicycleTheft() {
        return bicycleTheft;
    }

    public void setBicycleTheft(int bicycleTheft) {
        this.bicycleTheft = bicycleTheft;
    }

    public int getDrunkNDisorderly() {
        return drunkNDisorderly;
    }

    public void setDrunkNDisorderly(int drunkNDisorderly) {
        this.drunkNDisorderly = drunkNDisorderly;
    }

    public int getDrugUse() {
        return drugUse;
    }

    public void setDrugUse(int drugUse) {
        this.drugUse = drugUse;
    }

    public int getNonAggraBurgalary() {
        return nonAggraBurgalary;
    }

    public void setNonAggraBurgalary(int nonAggraBurgalary) {
        this.nonAggraBurgalary = nonAggraBurgalary;
    }

    public double getSafetyRate() {
        return safetyRate;
    }

    public void setSafetyRate(double safetyRate) {
        this.safetyRate = safetyRate;
    }
}
