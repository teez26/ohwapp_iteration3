package com.teamwings.benny.outofharmsway.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.teamwings.benny.myapplication.R;
import com.teamwings.benny.outofharmsway.adapter.ApiFetchJson;
import com.teamwings.benny.outofharmsway.bean.BicycleLocation;
import com.teamwings.benny.outofharmsway.bean.BicycleParking;
import com.teamwings.benny.outofharmsway.bean.ToiletLocation;
import com.teamwings.benny.outofharmsway.util.CommUtil;
import com.teamwings.benny.outofharmsway.util.LoadParkingCallback;
import com.teamwings.benny.outofharmsway.util.LoadParkingDataUtils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MapFragment extends Fragment implements OnMapReadyCallback {

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(getActivity(), "Map is Ready", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onMapReady: map is ready");
        mMap = googleMap;

        if (mLocationPermissionGranted) {
            getDeviceLocation();
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            init();
        }
    }
    private static final String TAG = "MapActivity";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private Boolean mLocationPermissionGranted = false;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private static final float DEFAULT_ZOOM = 15f;
    private EditText mSearchText;
    private ImageView mGps;
    public FragmentActivity myContext;
    private MapView gMapView;
    private SupportMapFragment mapFragment;
    private ImageView mParking;
    private ImageView mToilet;
    private Button clearBtn;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("Test", "Map fragment onCreateView");
        super.onCreateView(inflater, container,savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_map, null);
        mSearchText = view.findViewById(R.id.search);
        mGps = view.findViewById(R.id.ic_gps);
        mParking = view.findViewById(R.id.parking);
        mToilet = view.findViewById(R.id.toilet);
        clearBtn = view.findViewById(R.id.clearmarker);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // getSupportFragmentManager().findFragmentById(R.id.map1)
        mapFragment = (SupportMapFragment)this.getChildFragmentManager().findFragmentById(R.id.map1);

        mSearchText = view.findViewById(R.id.search);
        mGps = view.findViewById(R.id.ic_gps);
        mParking = view.findViewById(R.id.parking);
        mToilet = view.findViewById(R.id.toilet);
        clearBtn = view.findViewById(R.id.clearmarker);


        getLocationPermission();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    private void init() {

        Log.d(TAG, "init: initializing");
        mSearchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPlacePicker();
            }
        });
        mGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked gps icon");
                getDeviceLocation();
            }
        });
        mParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked parking icon");
                mMap.clear();
                buildParkingJSONApi();
            }
        });
        mToilet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked  icon");
                mMap.clear();
                buildToiletJSONApi();
            }
        });
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.clear();
            }
        });

    }

    private void loadPlacePicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), 100);
        } catch(GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == getActivity().RESULT_OK) {
                mMap.clear(); //clear all marker
                Place place = PlacePicker.getPlace(getContext(), data);
                // mark the place
                mMap.addMarker(new MarkerOptions().position(place.getLatLng())
                        .title(place.getName().toString()));
                moveCamera(place.getLatLng(), DEFAULT_ZOOM, "My location");
                filterNearbyParking(place.getLatLng());

            }
        }
    }


    private void geoLocate(){
        Log.d(TAG, "geoLocate: geoLocating");
        String searchString = mSearchText.getText().toString();
        Geocoder geocoder = new Geocoder(getActivity());
        List<Address> list = new ArrayList<>();
        try{
            list = geocoder.getFromLocationName(searchString, 1);
        }
        catch (IOException e){
            Log.e(TAG, "geolocate: IOException:" + e.getMessage());
        }
        if(list.size() > 0){
            Address address = list.get(0);
            Log.d(TAG,"geolocation: found a location " + address.toString());
            Toast.makeText(getActivity(), address.toString(), Toast.LENGTH_SHORT).show();
            moveCamera(new LatLng(address.getLatitude(), address.getLongitude()), DEFAULT_ZOOM, address.getAddressLine(0));
        }
    }

    private void getDeviceLocation(){
        Log.d(TAG,"getDeviveLocation: getting the device current location");
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        try{
            if(mLocationPermissionGranted){
                Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "onComplete: found location!");
                            Location currentLocation = (Location) task.getResult();
                            System.out.println("Your location latitude is" + currentLocation.getLatitude() + "," + "longtitude is "+ currentLocation.getLongitude());
                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),DEFAULT_ZOOM, "My location");
                        }
                        else {
                            Log.d(TAG, "onComplete: current location is null");
                            Toast.makeText(getActivity(), "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }

        }catch (SecurityException e){
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage());
        }
    }

    private void moveCamera(LatLng latLng, float zoom, String title) {
        Log.d(TAG, "moveCamera: moving the camera to : lat: " + latLng.latitude + ",lng:" + latLng.longitude);
        Log.d(TAG, "moving camera to "+latLng.toString());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        if (!title.equals("My location")) {
            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .title(title);
            mMap.addMarker(options);
        }

        //buildParkingJSONApi();
        // buildToiletJSONApi();




    }

    private void filterNearbyParking(final LatLng latLng){
        LoadParkingDataUtils.loadParkingJson(new LoadParkingCallback() {
            @Override
            public void loadSuccess(List<BicycleParking> list) {
                for (BicycleParking parking : list) {
                    BicycleLocation location = parking.getGeometry();
                    if(location != null && CommUtil.calDistance(latLng.latitude, latLng.longitude,
                            Double.valueOf(location.getCoordinates()[1]),
                            Double.valueOf(location.getCoordinates()[0])) <= 100){
                        // show location which is distance less than 100m
                        LatLng latLng = new LatLng(Double.valueOf(location.getCoordinates()[1]),
                                Double.valueOf(location.getCoordinates()[0]));
                        mMap.addMarker(new MarkerOptions().position(latLng).title(parking.getDescription()));
                    }
                }
            }

            @Override
            public void loadFailed(String errorMsg) {
                Toast.makeText(getContext(), errorMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void buildParkingJSONApi()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiFetchJson.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiFetchJson api = retrofit.create(ApiFetchJson.class);

        Call<List<BicycleParking>> call = api.getParkingData();
        call.enqueue(new Callback<List<BicycleParking>>() {
            @Override
            public void onResponse(Call<List<BicycleParking>> call, Response<List<BicycleParking>> response) {

                List<BicycleParking> bicycleParkings = response.body();

                for(BicycleParking bp : bicycleParkings)
                {
                    BicycleLocation parking = bp.getGeometry();
                    String loc = "lat/lng: ("+parking.getLatitude()+","+parking.getLongitude()+")";
                    LatLng llng = new LatLng(Double.parseDouble(parking.getLatitude()),Double.parseDouble(parking.getLongitude()));

//                Log.d("Location", String.valueOf(bp.getGeometry()));
                    Log.d("Name", bp.getGis_id());
                    Log.d("coordinates", parking.getLatitude()+ " "+ "//"+parking.getLongitude());


                    //adding markers for the parking locations.
                    MarkerOptions options = new MarkerOptions()
                            .position(llng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.parking_new))
                            .title(bp.getCondition_rating());
                    mMap.addMarker(options);
                }
            }

            @Override
            public void onFailure(Call<List<BicycleParking>> call, Throwable t) {

                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    public void buildToiletJSONApi()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiFetchJson.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiFetchJson api = retrofit.create(ApiFetchJson.class);

        Call<List<ToiletLocation>> call = api.getToiletData();
        call.enqueue(new Callback<List<ToiletLocation>>() {
            @Override
            public void onResponse(Call<List<ToiletLocation>> call, Response<List<ToiletLocation>> response) {

                List<ToiletLocation> toiletLocations = response.body();

                for(ToiletLocation tL : toiletLocations)
                {
                    LatLng latLng = new LatLng(Double.parseDouble(tL.getLat()), Double.parseDouble(tL.getLon()));

//                Log.d("Location", String.valueOf(bp.getGeometry()));
                    Log.d("Name", tL.getName());
                    Log.d("coordinates", tL.getLat()+ " "+ "//"+tL.getLon());


                    //adding markers for the parking locations.
                    MarkerOptions options = new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.toilet))
                            .title(tL.getName());
                    mMap.addMarker(options);
                }
            }

            @Override
            public void onFailure(Call<List<ToiletLocation>> call, Throwable t) {

                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    private void initMap(){
        Log.d(TAG, "initMap: initializing map");
//        SupportMapFragment mapFragment = (SupportMapFragment) .getSupportFragmentManager().findFragmentById(R.id.map1);

        mapFragment.getMapAsync(this);
    }

    private void getLocationPermission(){
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(this.getActivity(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getActivity(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                mLocationPermissionGranted = true;
                initMap();
            }
        }
        else {
            ActivityCompat.requestPermissions(getActivity(), permission,LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionResult: called.");
        mLocationPermissionGranted = false;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if (grantResults.length > 0 ){
                    for (int i = 0; i < grantResults.length; i++){
                        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionGranted = false;
                            Log.d(TAG, "onRequestPermissionResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionResult: permission granted");
                    mLocationPermissionGranted = true;
                    initMap();
                }
            }
        }
    }

}







