package com.teamwings.benny.outofharmsway.bean;

public class ToiletLocation {

    private String baby_facil;
    private String female;
    private String lat;
    private String lon;
    private String male;
    private String name;
    private String operator;
    private String wheelchair;

    //constructor
    public ToiletLocation(String baby_facil, String female, String lat, String lon, String male, String name, String operator, String wheelchair) {
        this.baby_facil = baby_facil;
        this.female = female;
        this.lat = lat;
        this.lon = lon;
        this.male = male;
        this.name = name;
        this.operator = operator;
        this.wheelchair = wheelchair;
    }

    //gettter


    public String getBaby_facil() {
        return baby_facil;
    }

    public String getFemale() {
        return female;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getMale() {
        return male;
    }

    public String getName() {
        return name;
    }

    public String getOperator() {
        return operator;
    }

    public String getWheelchair() {
        return wheelchair;
    }
}


/*
{"baby_facil":"no",
        "female":"yes",
        "lat":"-37.806121499077804",
        "lon":"144.95653844268273",
        "male":"yes",
        "name":"Public Toilet - Queen Victoria Market (153 Victoria Street)",
        "operator":"City of Melbourne",
        "wheelchair":"no"}*/
